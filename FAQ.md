## Does this CMS run with PHP 7?

Yes it does, with 7.0. We do not guarantee compatibility to higher versions.

## Will you update to Laravel 5.5 / Bootstrap 4?

Yes. Most likely with Contentify 3.

## How can I clear the cache?

You have three options:

1. Via admin control panel. Since Contentify 2.4 there is a "Clear Cache" button in the config page in the backend.

2. Via console (terminal or SSH). Switch to the Contentify directory, then execute `php artisan cache:clear`.

3. Manually. Delete all files and directories in `storage/framework/cache`, `storage/framework/sessions` and `storage/framework/views`.

## How can I disable the debug mode?

Open the file `config/app.php` with a text editor. Now find this code (typically in line 39):
```
    'debug' => env('APP_DEBUG', true),
```
Replace `true` with `false`.

## How can I move the website to another location?

1. Export the old database and import it in the new location
2. Copy all files in the all location and paste them in the new location
3. If necessary, edit the database connection details in `storage/app/database.ini`
4. Check the directory permissions (CHMOD 777)
5. Create the cron job
6. Clear the cache

## How can I fight spambots?

Per default users that register are activated right away. You can change this behaviour: Open `app/Modules/Auth/Http/Controllers/RegistrationController.php` and set `AUTO_ACTIVATE` to false. If set to false, an admin has to activate new users manually. To activate a user, switch to the admin interface, open the users module page, find the user, click on the "edit" button and set the "activated" checkbox on true.

## Does this CMS use a frontend framework?

No, it does not use a frontend framework such as AngularJS. It uses jQuery and a library with some helper functions.

## How can I add more social logins?

Steam logins is built-in but nothing else. Nevertheless the CMS is well prepared. You may require the `Cartalyst\SentinelSocial` or `Laravel\Socialite` package to get third party authentication support. Take a look at the official docs and/or watch this very helpful video tutorial: https://laracasts.com/series/whats-new-in-laravel-5/episodes/9

> NOTE: `Cartalyst\SentinelSocial` is not open source so you would have to pay for it. 

## Do you plan to extend language support?

Yes, we plan to extend language support by adding more interface languages. We do not plan to allow translation of contents. Also, English is the primary language. Very few text strings may not be translated at all.

## How can I generate all the favicons?

There are a lot of possible (fav)icons you may want to generate. If you have NodeJS and NPM installed, the [favicons](https://www.npmjs.com/package/favicons) package might help you out.

## How can I enable the STEAM login?

2. In your browser, navigate to the STEAM API key website: https://steamcommunity.com/dev/apikey
3. Login and then copy your API key
4. Open the file `config/steam-auth.php` with a text editor
5. Paste your key in the `api_key` section and save the file

## How can I embed Twitch Streams?

Contentfiy has a module called `Streams` that supports the embedment of live streams. Twitch is on of the streaming platforms that are supported out-of-the-box. Unfortunately, Twitch decided at the 5th of May 2016 to [enforce the use of an API key](https://blog.twitch.tv/client-id-required-for-kraken-api-calls-afbb8e95f84) for requests to their API (called "Kraken"). Since then Contentify cannot connect to Kraken without _you_ setting up an API key. Sorry for the inconvenience - but please blame Twitch, not us. There is no work-around, you have to tell the CMS your Twitch key. These are the steps:

* Create a Twitch account or log in if you already have one
* [Register a new application](https://www.twitch.tv/kraken/oauth2/clients/new). Then copy the value of the field "Client-ID". This is your Twitch API key.
* Go to your Contentify website, log in, go to the admin interface, then go to the settings module and insert the key in the "Twitch API Key" field.

## How can I use Google ReCAPTCHA instead of the default captcha?

Please follow these instructions:

1. Go to the [Google ReCAPTCHA website](https://www.google.com/recaptcha/admin) and create your ReCAPTCHA.
2. Copy & paste the JS script code into the main layout: `app/Modules/<ThemeName>/Resources/Views/layout_main.blade.php`
3. Open the proper template. For example if you want to use ReCAPTCHA in the registration template: `app/Modules/Auth/Resources/Views/register.blade.php`. Replace the code for the default captcha with the code from the Google ReCAPTCHA website. Maybe you will want to adjust the layout of it a bit to make it look better.
4. Open the controller of the template. In this case, open `app/Modules/Auth/Http/Controllers/RegistrationController.php`. Find `Captcha::check(Input::get('captcha'))` and replace it with `Captcha::checkReCaptcha(Input::get('g-recaptcha-response'))`.
5. Open the config file `config/app.php` and set the value of the `recaptcha_secret` key to the server secret from the Google ReCAPTCHA website.

## Is there a desktop application?

No, we do not offer a desktop application that you can use to control your website. But if you really want to have one, we encourage you to try Nativefier:

1. Go to their website: https://github.com/jiahaog/nativefier
2. Download and follow their guide
3. Make a desktop applicatino of your website

_Disclaimer_: We do not offer any support for Nativefier so if you try this, you have to solve problems on your own.

## Who is this daemon user?

The user with the ID 1 and the name `daemon` is created by the installer. It's a dummy user that is some kind of a representative of the CMS. It is not accessible, for no one. It does not have admin permissions. Please do not delete this user.

## How can I contact you?

In genereally you can reach us via:

* [GitHub Issue](https://github.com/Contentify/Contentify/issues) _(recommended)_
* [Email](https://github.com/Contentify)
* [Gitter](https://gitter.im/Contentify/Lobby)
* [Twitter](https://twitter.com/ContentifyCMS)
* [Facebook](https://www.facebook.com/contentifycms/)