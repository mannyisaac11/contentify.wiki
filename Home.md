Welcome to the Contentify wiki!

## Important Chapters

* [Installation](Installation)
* [FAQ](FAQ)
* [Troubleshooting](Troubleshooting)
* [Laravel](Laravel)
* [Website Development](Website-Development)

Questions? Create an [issue](https://github.com/Contentify/Contentify/issues) please!

## All Chapters

* [Installation](Installation)
* [FAQ](FAQ)
* [Troubleshooting](Troubleshooting)

***

* [Laravel](Laravel)
* [Differences To Laravel](Differences-To-Laravel)
* [Conventions](Conventions)
* [Website Development](Website-Development)
* [Creating Layouts](Creating-Layouts)
* [Themes](Themes)
* [Modules](Modules)
* [Helper Functions](Helper-Functions)

***

* [Artisan](Artisan)
* [Blade](Blade)
* [Controllers](Controllers)
* [Model Handler](Model-Handler)
* [Module Development](Module-Development)
* [Module Routing](Module-Routing)
* [Widgets](Widgets)
* [Localisation](Localisation)
* [JavaScript](JavaScript)
* [Optimization](Optimization)
* [Going Live](Going-Live)

***

## Special Thanks

These persons added a notable contribution to the CMS:

* **krysennn**: French translation