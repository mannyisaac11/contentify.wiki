## Base Controller

All controllers should inherit from the `BaseController` class. Its constructor adds several attributes such as the module and controller name and the model that is handled by the inheriting controller. It also enables CSRF protection. The base controller class implements several methods that are extremly helpful when working with controllers.

* *pageView*: Adds a view to the main layout
* *pageOutput*: Adds a string to the main layout
* *pageMessage*: Adds a message view to the main layout
* *alertFlash*: Inserts an alert to the main layout that is displayed at the _current_ response
* *alertFlash*: Inserts a flash alert to the main layout that is displayed at the _next_ response
* *indexPage*: Builds an index page from a model
* *metaTag*: Adds a HTML meta tag to the main layout
* *title*: Sets the HTML title tag of the main layout
* *openGraph*: Binds an OpenGraph object to the main layout
* *breadcrumb*: Sets the links for the breadcrumb navigation
* *setupLayout*: Laravel 4 method to set a "controller view". Laravel 5 does not support this, but Contentify does.
* Permission helper functions

### indexPage()

This calls the `index()` method of the [model handler](Model Handler).

## Frontend Controller

Extends the BaseController class. Controllers tied to the frontend should inherit from the `FrontController` class. The frontend controller sets the frontend layout as the main template. Its constructor passes variables (module and controller name) to this template. It also implements a search method.

## Backend Controller

Extends the BaseController class. Controllers tied to the backend should inherit from the BackController class. While the frontend controller class is lightweight, the backend controller comes with a lot of features. It sets the backend layout as the main template and passes several variables to it (module and controller name, controller icon, user picture, message notifier).

The most outstanding feature is its ability to handle resource actions by utilizing the [model handler](Model Handler). It's capable of using a trait called `ModelHandlerTrait` that implements methods that create (and store), edit (and update) and delete (and restore) a model. This is well known as CRUD - eventhough it's flavoured CRUD. Controllers that use the `ModelHandlerTrait` always inherit these methods. Keep this in mind when creating routes for a resource controller. Perhaps you have to disable routes manually that you do not want to use. For example maybe your controller must not be able to delete the models it handles. In this case you have to close the "delete" route manually.

> Use the `php artisan route:list` command to list all available routes.

If a method does not work in the way it's intended or the task to perform is too complex you are free to override it. But if it isn't necessary to override it's recommended to stick to the existing implementation.

## Modifying ModelHandlerTrait Methods

The simplest way to implement another behaviour for a CRUD action is to rename the method and to create a new one:

    // app/Http/Controllers/ExampleController.php that extends BackController

    use ModelHandlerTrait {
        create as traitCreate;
    }

    public function create()
    {
        // Your implementation
    }

Ofcourse you are still able to call the method of the ModelHandlerTrait class:

    public function create()
    {
        // Do stuff before...

        $this->traitCreate();

        // ...and after calling the method of the ModelHandlerTrait.
    }

It's possible to add extra data to the view that's created by a ModelHandlerTrait method:

    public function create()
    {
        $this->traitCreate();

        $games = Game::all();

        $this->layout->page->with('games');
    }