> Themes are only available in Contentify 2!

Contentify 2 introduces an important new feature: Themes. Your website can have as many themes as you want to and you can switch between them just by choosing another theme on the settings page.

## Creating Themes

Themes are [modules](Modules). Modules with some sugar, but still modules. They only differ in very few things:

* The name of the module has to end with "Theme". For example: "DragonTheme"
* Its `module.json` config file has to have an attribute with the name `theme` and the value `true`: `"theme": true`
* The module has to have a view (template) with then name `layout.blade.php`. This is the main layout template.

If you want to create a new theme we recommnd that you copy & paste the folder of an existing theme (maybe the default theme?) and modify it. Since themes are modules they have access to the whole power of modules! This means they can publish files to the public folder. Especially CSS files. You should create a subfolder named `Assets/css` and use it to store the CSS files of your theme. Then open the service provider of your theme with a text editor. According to our example this would be `app/Modules/DragonTheme/Providers/DragonThemeServiceProvider.php`. Then add or modify the the `boot` method so it looks like this (obviously, you do not need to to that if you copied a module that already publishes its CSS files):

    public function boot()
    {
        $this->publishes([
            __DIR__.'/../Resources/Assets/css' => public_path('css'),
        ], $this->namespace);
    }

That's it! Remember that you can publish any type of files (JavaScript, images...) in the same way. Remember that themes are modules! You can add controllers, [widgets](Widgets), classes... whatever kind of additional PHP code you might need, it's easy to add it to your theme!

To make developing themes more convenient the default themes have a `gruntfile.js` in their `Resources` directory. It compiles the `Resources/Assets/less/*.less` files to `.css` files in the `<theme>/Resource/Assets/css` directory and in the `<cms>/public/css` directory. To use Grunt, you havew to install Node.js and the Grunt plugin. Afterwards you can open a console, switch to the `<theme>/Resource` directory and type the `grunt` command to run the `gruntfile.js` script.