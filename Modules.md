> You want to know how to develop modules? There is an own chapter about this topic: [Module Development](Module-Development)

## Slides

If you want to enhance your website with a real eyecatcher a slider is your method of choice. A slider consists of slides. To maintain continuity sliders are called "slide categories" in the backend, so every slide belongs to a slide category. Many JavaScript sliders exist out there but for your convenience Contentify comes with a slider already built-in. Inserting it into a template is a piece of cake:

    @widget('Slides::Slides', ['categoryId' => 1])

`categoryId` is the ID of the corresponding slide category aka the slider. You may add multiple slider widgets to one template. The widget basically adds the `modules/slides/views/widget.blade.php` template to the parent template. That's where the jQuery slider plugin is instanciated, like so:

    $('#slider{{ $categoryId }}').contentifySlider();

It's possible to pass options to it - take a look at `libs/slider.js` for a complete list:

    $('#slider{{ $categoryId }}').contentifySlider({ autoplay: false });

The slider widget comes with a default CSS styling. Don't hesistate to make any changes you want to adapt the styling to your website. It's located in the `frontend.css` stylesheet at the `.slider` class. You probably want to set the slider's height to an appropriate value.

    .slider { position: relative; width: 100%; height: 245px; /* Height of a single slide */ }

## Scrollers

If you just need a UI container with items that can slide horizontally consider to implement a scroller insteaf of a slider. A scroller is a HTML container with a list of items and two scrolling buttons. Clicking on one of the buttons slides to the previous or the next item. Endless sliding in one direction is possible. Example markup:

    <div class="scroller">
        <ul class="layout-h">
            <li>
                Item 1
            </li>
            <li>
                Item 2
            </li>
        </ul>
        <a class="to-left" href="#">&lt;</a>
        <a class="to-right" href="#">&gt;</a>
    </div>

    {{ HTML::script('libs/scroller.js') }}
    <script>
        $(document).ready(function()
        {
            $('.scroller').contentifyScroller();
        });
    </script>

It's possible to display content with a JS scroller instead of a JS slider and vice versa.

## Videos

Every video belongs to a provider. The only two default providers are `youtube` and `vimeo`. As you might guess, a provider is a platform that hosts videos. The `App\Modules\Videos\Video` model stores a `$providers` array with available providers. You may add aditional providers to this array. If you do so you have to care for handling videos from these providers by modifying corresponding templates - at least `app/modules/videos/resources/views/index.blade.php` and `show.blade.php`. You may also take a look at `admin_form.blade.php`.

## Streams

Streams - most likely hosted by [Twitch](http://twitch.tv/) or [Smashcast](http://smashcast.tv/) - usually tend to be online or offline and to have at least zero viewers. The `Streams` module can display such information but it needs to frequently update them. Therefore it adds a job to the jobs manager. This job will be executed each time when the job manager is triggered by a cron job and the job is not cooling down. Therefore you need to create a cron job if you want automated updates. Read more at the [GitHub page of the Jobs package](https://github.com/chriskonnertz/Jobs).

## Navigations

As an administrator you are able to create new navigations (=menus) in the backend. If you do so, you have to manually include these navigations to one or more templates. Probably you want to add it to the main template, so open `app/Modules/<ThemeName>/Resources/Views/layout.blade.php` with a text editor and add this code to an appropiate place: `@widget('Navigations::Navigation', ['id' => 123])` Do not forget to replace 123 with the actual ID of the navigation.

## Pages

Pages are containers for rich-texts. There are three types of pages:

- **Blog Post**: Also called "articles". They have a comment section.
- **Page**: Similar to blog posts, but they do not have a comment section.
- **Custom Content**: They are meant to be included somehwere else, for example you may use them as editor templates and add them to texts by clicking on the "insert template" button. You may also include them on your website with the help of the `Pages::Fragment` widget. This allows you to create parts of the website that can easily be modified in the backend.

## Shoutbox

The "Shouts" module focuses on a single widget, the shout box widget. It is a very simple chat for your website.

> Technically the shout box is pretty rudimentary. Therefore it might not perform well on websites with much traffic. We do not recommend to use it on a page of your website that is accessible by everyone (for example the landing page) if your websites has more than 1000 daily visitors.

Use the shout box widget by adding this code to a Blade template: `@widget('Shouts::Shouts')`

## Contact / Applications

The "Contact" modules consists of two parts: Its main part is to provide a contact form when a client navigates to the URL `contact`. Its secondary part is the application form. Navigate to `application` to see this form. You will be able to see messages of both types in the backend in the "contact" section.

## Languages

The CMS supports multiple interface language. Users can choose one of these languages by editing their user settings. It is also possible to add a widget that displays country flags of countries associated with the languages. Users can click on a flag to choose a language. Add the widget to a Blade layout with this code: `@widget('Languages::Languages')`

## Users

### Widgets

The "User" module has several widgets:

- Latest user widget: `@widget('Users::LatestUsers')` Displays the latest users, ordered by date & time when they created their account. This widget has one optional parameter: `limit` This parameter specifies how many users are displayed. Technical hint: This widget uses a HTML table element which a header and one row for each user. This widget is used in the backend in the dashboard.
- Online widget: `@widget('Users::Online')` Displays the users who are currently online on your website. Users are considered to be "online" within 5 minutes after their last action. This widget has one optional parameter: `limit` This parameter specifies how many users are displayed. 
- Random user widget: `@widget('Users::RandomUser'`) Displays one randomly selected user. Also known as "fame box". Picks a new user after 60 minutes. If you want to change that, use the `minutes` parameter.