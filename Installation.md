# Server Requirements

* PHP 5.6.4 or higher
* OpenSSL PHP Extension
* Mbstring PHP Extension
* Tokenizer PHP Extension
* MySQL with one database

It's recommended to activate Apache's mod_rewrite module (or Nginx' HttpRewriteModule). To activate mod_rewrite on a Debian destribution, execute `sudo a2enmod rewrite`. Sometimes you also have to allow the `.htaccess` file to override standard website configs.

We strongly recommend to use webspace that you can configure. You should be able to create vhosts at least. And there is a command-line interface called [Artisan](http://laravel.com/docs/artisan). You should ensure you can run it. With Artisan you are able to [activate the maintenance mode](http://laravel.com/docs/configuration#maintenance-mode).

# Hosting

If you do not have a hosting service so far or it does not meet the requirements listed above we suggest to give [DigitalOcean](http://www.digitalocean.com) a try. They offer an SSD server with 512 MB RAM and 20 GB disk space for only 5 USD per month.

* Create a new droplet with Ubuntu. Choose to install the LAMP (Linux, Apache, MySQL and PHP) stack.
* Connect to your server using an SSH client (e. g. [PuTTY](http://www.putty.org) for Windows)
* Download the script: `wget http://contentify.org/share/install.sh`
* Run the script: `sudo bash install.sh`

> There is a video tutorial about the installation: [Watch it on YouTube](https://youtu.be/-DZpdQ7XoIo)

# Install Missing PHP Extensions

If [PHP extensions](#server-requirements) are missing, you have to install them. The CMS cannot run without them being installed. Unfortunately installing PHP extensions can be a challenge. Therefore here are some hints.

The first step on your Windows environment is to open `php\php.ini` (if you are using XAMPP it's `<xammp>\php\php.ini`) and to search for the name of the extension like so: `;extension=php_fileinfo.dll` If you find this line remove the semicolon to activate the extension. If you don't find it, you need to download the extension first and then add it to the `php.ini`. 

On Linux-based environments it depends on your distribution. Perhaps you are able to install the mod via `apt-get`. For example if you use PHP 7 on Ubuntu you may install the mbstring extension with this command: `sudo apt-get install php7.0-mbstring` If you need to install the libxml package use this command: `sudo apt-get install php7.0-xml`

# Get Contentify

Download the latest Contentify core files from [our GitHub page](https://github.com/Contentify/Contentify/releases). Store the files inside a folder so that only the subfolder `public` is accessible from the web. If only `public` is accessible from outside all other folders are protected from direct access. When on Apache you may want to [create a Virtual Host](http://laravel-recipes.com/recipes/25) for your project to achieve this. The aim is to have a URL like `http://localhost/contentify/` instead of `http://localhost/contentify/public/`. For testing or developing purposes it's okay to put the whole CMS folder inside the public web folder. But remember, this is not meant for production stage! Another alternative is to [use a `.htaccess` file](https://github.com/Contentify/Contentify/wiki/Troubleshooting#installation-what-to-do-with-the-public-folder).

# Configuration

Config files live in the `config` directory. Important config files are:

* *app.php*: Application settings such as title and encryption key
* *database.php*: Database settings such as connection setup. It's recommended to set `utf8_unicode_ci` as collation when you create the database. `utf8_general_ci` will work but [sorting will be inaccurate](http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci#766996). NOTE: The database connection settings are stored in `storage/app/database.ini`. The installer will ask your for these settings and write them to this file.
* *mail.php*: Settings such as name and address for all e-mails that are sent by your application.

# Installation

* (Download Contentify and configurate it)
* Set CHMOD 777 to these directories and their sub directories and all files in these directories: `<contentify>/storage`, `<contentify>/bootstrap/cache`, `<contentify>/public/uploads`, `<contentify>/public/share`, and `<contentify>/public/rss`
* In your browser, navigate to the website to run the installer. Example URL: `http://localhost/contentify/install.php` (or `http://localhost/contentify/public/install.php` if `public` is not the root)

The official Laravel docs have a [chapter covering the installation](http://laravel.com/docs/5.3/installation).

# Create Cron Job

To unleash the full potential of the CMS you have to ceate a cron job. On Linux, run the `crontab- e` command and then add this entry to the cron job file: 

```1 * * * * php /var/www/laravel/artisan jobs```

This will execute the Artisan command every minute. We highly recommend to run the cron job every minute.

# Preparation For Production

Per default, Contentify is prepared for development. To prepare it for a production environment, disable the debug mode in `config/app.php`. Also ensure that the application key (you will find it in the same file) has been set to a random string. Contentifys's diagnostic module offers a convenient way to check if everything is set to proper values. Navigate to the backend dashboard and click on the diagnostics icon to open the diagnostics page.

# Something Is Going Wrong?

Installing Laravel can be a little tricky. If you experience problems take a look at the [Troubleshooting](Troubleshooting) chapter. If the problem isn't covered don't hesistate to contact us on [GitHub](https://github.com/Contentify/Contentify/issues).