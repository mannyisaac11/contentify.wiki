If you want to use a website on a production server (live server / server that's accessible to the public) follow these steps:

* Ensure sure the encryption key is set to a random value. It's located in the `config/app` file with the key name `key`.
* Also disable debug mode if it's activated (and disable Xdebug if it's enabled)
* Create a cronjob that is executed once per minute and calls the `jobs` [Artisan](http://laravel.com/docs/artisan) command. Example: `1 * * * * /usr/bin/php /var/www/laravel/artisan jobs`
* Finally, check the diagnostics module (e. g. `http://<server>/diag`) to reveal possible issues.

Additionally you may consider to minimize (and concat) JS and CSS files.