## Date And Time

Please use [Carbon](https://github.com/briannesbitt/Carbon) instead of the native DateTime class. Carbon extends DateTime and adds lots of fantastic features including localisation support. For all examples the use of Carbon is assumed.

Carbon defines the `__toString`method. This allows a pretty short way to print a date:

    echo $news->created_at; // Example output: 1969-07-21

Or use the `date` method to receive the same output. Call `dateTime` to receive date and time:

    echo $news->created_at->dateTime(); // Example output: 1969-07-21 13:37:00

To localise a timestamp (that is not an instance of Carbon but an integer), try this:

    date(trans('app.date_format'), $timestamp)

## Eloquent Models And Carbon

Eloquent converts only certain model attributes to Carbon instances:

* **created_at**
* **updated_at**

Override the `$date` array of a model to customize which attributes are mutated:

    protected $dates = ['created_at', 'updated_at', 'deleted_at', 'baked_at'];

## Add more languages

* *Step 1*: The `resources/lang` directory contains the languages files that are not part of a module. The `resources/lang/<code>/app.php` file is the most important language file, because it contains date format specifications. To add a new language, copy the an existing language directory (for instance `resources/lang/en') and name it to the language code of the new language (here is a [list of ISO language codes](http://www.w3schools.com/tags/ref_language_codes.asp)). Afterwards you can translate the files.
* *Step 2*: Modules can have language directories as well. Browse then modules and if they have a `lang` directory perform the same actions as in step 1.
* *Step 3*: Update the `languages` databe table. This table is the connection between the language directories and the user interface. To add a new language, add a new record with the title and the code of the language.

If you do not want to acutally translate a language but only change the date format specifications, add a new language but only create the `resources/lang/<code>/app.php` file and add no other content that the date items (`date_format` and `date_format_alt`). The default language for not existing keys in a language file is English.