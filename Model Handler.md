## Model Handler

The model handler is a class that takes care of frequently occurring tasks related to models. It's a very important and powerful part of Contentify. It helps to avoid writing a lot of code when dealing with models.

### index()

This method returns a string with HTML code that renders an index page. Controllers usually handle a single model. These resource controllers help to build RESTful controllers around resources. The index method implements the "index" action. You can pass a data array to configure how this page is rendered.

Basic example (inside a controller that extends `BaseController`):

    $data = [
        'tableHead' => [
            trans('ID')     => 'id', 
            trans('Title')  => 'title'
        ],
        'tableRow' => function($game)
        {
            return [
                $game->id,
                $game->title,
            ];            
        }
    ];
    $this->indexPage($data);

The main part of the every index page is a table showing entities. In the example the table has two header columns ("ID" and "Title"). Both are related to an attribute of the entity which make them sortable. The table body consist of rows. To build each row a Closure is executed. It recieves the current entity as a parameter and returns an array with values for the columns.

List of arguments the data array may use:

* *buttons*: Array of names of default buttons ("new" and "category") or custom HTML code
* *searchFor*: The attribute to search (e. g. "title"). Null will disable the search feature.
* *infoText*: Info text that is displayed above the table. You may use HTML tags.
* *dataSource*: Null or array of entities. If null it will take the entities from the database. If an array is passed sorting, searching and sorting are not available.
* *tableHead*: Array of items for the table head columns
* *tableRow*: Closure returning an array of items for all columns of a single row
* *actions*: Array of named action buttons for the "Action" column ("edit", "delete" or "restore") or Closures
* *brightenFirst*: Display the first colum values in a bright color? (useful to display IDs in a different style)
* *sortby*: Name of the model attribute used for default sorting (e. g. "id")
* *order*: Default order ("asc" or "desc")
* *filter*: Boolean. If there is content filter UI, apply it to the models?
* *permaFilter*: Null or Closure. The model query is passed to the Closure so additional Eloquent manipulations (e. g. filtering with where clauses) is possible. The Closure must return the query.