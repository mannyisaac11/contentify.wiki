Modules are Laravel packages with some sugar. If you prefer pure packages there is no reason why you shouldn't use them over modules. But modules have advantages too. They are easier to create and they may have an installer. Internally a package called [Caffeinated Modules](https://github.com/caffeinated/modules) implements the module support.

## Installing New Modules

1. Copy and paste the folder of the module to `app/Modules`.
2. Delete this file: `storage/app/modules.json` This file is a cache file  with infos about the modules and it needs to be refreshed. When you delete it, it will be re-generated automatically.
3. Clear the cache to see new entries in the navigation of the backend. The easiest way is to delete all folders in `storage/framework/cache`.
4. Go to the backend, "Website", "Modules" and find out if the modules needs to be installed (plus button in the "Actions" column). If so, install it.

## Module Files

Modules live in their own folders in `app/Modules`. Example folder structure:

    Http/
        Controllers/
            AdminGamesController.php
        web.php
    Providers/
        FriendsServiceProvider.php
        RouteServiceProvider.php
    Resources/
        Lang/
        Views/
            admin_form.blade.php
    Game.php
    module.json

Module folders usually have at least three subfolders: `Http`, `Providers` and `Resources`. Yep, these are inherited from the `app` directory structure. They are not mandatory. You can add your own subfolders if you like to. 

PHP files that handle backend stuff should have an "admin" prefix.

 `module.json` is a config file containing the configuration of the module. Minimum example file:

    {
        "Slug": "Games",
        "enabled": true
    }

Possible properties are:

* *slug* (string): The name of the module
* *enabled* (boolean): Enables or disables the module
* *search* (array): Array with names of controllers that support the global search
* *admin-nav* (array): Array of arrays that describe items of the backend module navigation. Supported properties are (all of them are optional):
    * *icon* (string): Filename of an icon (default: `newspaper.png`)
    * *category* (int): ID of the navigation category (default: 1)
    * *position* (int): Position inside the navigation category (default: 999)
    * *title* (string): Title of the navigation item (default: name of the module)
    * *translate* (boolean/string): If `true` (default), try to auto-translate the title. If `false`, do not translate the title. If string, try to find a translation for this key (do not forget to specify a namespace!) and use it as the title.
    * *url* (string): URL to the controller (default: `admin/<module-name>`)

The `Http/routes.php` file is some kind of a clone of `app/Http/routes.php`. But since this is a module Contentify has a helper class called `ModuleRoute`. It creates some shortcuts to routing functions. Example file:

    ModuleRoute::context(__DIR__);

    ModuleRoute::resource('admin/games', 'AdminGamesController');
    ModuleRoute::post('admin/games/search', 'AdminGamesController@search');

The context method sets the module context to the current module. This is essential to notify the helper about the module it' i's working with. When calling methods such as resource the helper will know to which module they belong. Read more in [the module routing chapter](Module Routing). Ofcourse, Laravel's `Route` class is still available.

## Module Specifics

Whenever a Laravel method is called that accesses data that is tied to a module `::` is the seperator.

    // Recieve line "name" from app/modules/cookies/lang/../cake.php:
    $name = trans('cookies::cake.name'); 

    // Make view from app/modules/cookies/view/cookie.php:
    $view = View::make('cookies::cookie');

## Module Installer

Modules may have their own installer if they need to run a setup process. The installer is a class extending the `ModuleInstaller` class. It lives in the root of its module folder in the `Installer.php` file.

The execute method of the installer may return three kinds of values:

* *true*: Installation completed
* *false*: Installation terminated (due to an error)
* *string* (or View): Visual output 

If the return values is not a boolean it will be added to the page output. You may use this to render forms if the installer needs to request user input. This fictional example creates a view that renders a form with a single field `tableName` at step 0. The form action is set to the URL of the next step. In step 1 the installer takes the field value and creates a table with the custom name. At the end of step 1 the method returns true to indicate the end of the installation.

    namespace App\Modules\Example;

    use View, ModuleInstaller;

    class Installer extends ModuleInstaller {

        public function execute($step)
        {
            if ($step == 0) {
                $url = $this->nextStep();

                return View::make('cookies::cookie_installer', $url);
            } else {
                $tableName = Input::get('tableName', 'cookies');

                Schema::create($tableName, function($table)
                {
                    $table->increments('id');
                    // ...
                });

                return true;
            }
        }

    }

Did you notice the `nextStep` method? This is a method provided by the base class. Its a helper and returns the URL of the "next step" of the current installation. If passed to a view its possible to create a link the user has to click to proceed to the next step.

To start the installation open the "modules" module in the backend. It will display an install button near to modules that have an installer.

### Module Permissions

Accessing a backend module always needs privileges. They are stored in the `roles` table in the database. For default modules they are added during the installation of the CMS but of course that is not the case for a new module. One way to deal with this is to add an installer to the module. The "Example" module shows an example for such an installer: `app/Modules/Example/Installer.php` You can just copy it and change the namespace. Installation should be available then and automatically add permissions for super-admins. If you want to set more detailed permissions you can use the `extraPermissions` property wich is inherited from the `Contentify/ModuleInstaller` class. See: https://github.com/Contentify/Contentify/blob/2.1-dev/contentify/ModuleInstaller.php#L36