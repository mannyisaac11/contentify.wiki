This chapter deals with solutions for known errors and issues.

## Installation: Error message "FatalErrorException in Migrator.php"

No worries if this happens when trying to reinstall Contentify. Something is wrong with your migration table. Usually the name of this table is `migrations`. Truncate or delete it manually, then repeat the installation. If it still does not work, delete all database tables of Contentify, then repeat the installation.

## Installation: What to do with the public folder?

Set it as the document root in your Apache configuration. Open your Apache config file, usually `/etc/apache2/sites-available/000-default.conf` with a text editor such as Nano. You may install Nano via `sudo apt-get install nano`. Then find the line that sets the document root, usually it looks like this: `DocumentRoot /var/www/html` Now change the path to the path of the `public` directory, for example: `DocumentRoot /var/www/html/public`. Save the file and restart the Apache server via `sudo service apache2 restart`.

If this is not possible - for example, because you are not allowed to change this setting - you have to create a `.htaccess` file and upload it to the root folder of Contentify (=the folder that containts the `public`folder). This has to be the content of this file:

```
RewriteEngine on
RewriteCond %{HTTP_HOST} ^domain-name.com$ [NC,OR]
RewriteCond %{HTTP_HOST} ^www.domain-name.com$
RewriteCond %{REQUEST_URI} !public/
RewriteRule (.*) /public/$1 [L]
```

Replace the following 2 strings:

* domain-name.com - Insert your own domain name
* folder - Type the name of the sub-folder which has the test/development website

_Thank you @batafly5 for this hint_

## I cannot find the `/public/install` directory

That's true - because it is not a real "physical" directory on your hard disk. It is just a route. It looks like a path to of a directory but actually it is not, it is just a URL that with help of a [.htaccess](https://github.com/Contentify/Contentify/blob/2.2/public/.htaccess) file is internally routed to a PHP controller script.

If you cannot access this URL via a web browser than your server is not configurated correctly. When using Apache please make sure that URL rewriting is enabled. To activate mod_rewrite on a Debian destribution, execute `sudo a2enmod rewrite`. Sometimes you also have to allow the `.htaccess` file to override standard website configs.

## Installation: execution time exceeded

If PHP cancels the database setup with a "Maximum execution time of 30 seconds exceeded" exception this means creating the database is too time-consuming. Unfortunately creating the database is **very** time-consuming. If it's running on systems with little computing power the script can exceed the execution time limit. There are two solutions:

* Try to increase the excution time: Open the `php.ini` config file and set [max_execution_time](http://php.net/manual/de/info.configuration.php#ini.max-execution-time) to an appropriate value (or simply set it to 0).
* Use the link below that brings you to a gist with an SQL dump file with a database that's ready for use. Import this file, for example with [phpMyAdmin](http://www.phpmyadmin.net/).
    * v2.3: https://gist.github.com/chriskonnertz/baa6747339d16e058c97c587f6b66cd5
    * v1.2: https://gist.github.com/chriskonnertz/77cee70e7599e48872ffd98b6d9ec4ab

## PHP: maximum function nesting level reached

The exception (e. g. "Maximum function nesting level of '100' reached, aborting!") is caused by XDebug. It's not a bug of Laravel or the CMS. Open your `php.ini` and change (or add if it does not exist) `xdebug.max_nesting_level` to a value higher than 100, for instance 512 (that's what [Laravel Homestead](https://github.com/laravel/settler/blob/master/scripts/provision.sh#L125) uses), or update XDebug since it uses a higher default value in newer versions.

## CSRF token missmatch exception

Make sure the `upload_max_filesize` PHP config value is set to a value that matches the size of the file you try to upload. Also ensure `upload_max_filesize` is not higher than `post_max_size`. If so, open the `php.ini` and set both value to the same value that at least has the same size as the size of your upload.

## Time / date problems

Navigate to your website. Login and go to the admin backend. In the dashboard, click on "Config". Then click on the diagnostics button. Make sure the PHP server timezone (`PHP.date.timezone`) matches with the timezone of the CMS (`App.timezone`). The timezone of the CMS is UTC by default. If it differs from the timezone of your server strange things might happen. Here is a list of supported timezones: [PHP Docs](http://php.net/manual/en/timezones.php)

## MySQL: foreign key constraint fails

If a "foreign key constraint fails" exception is thrown, check if Eloquent is trieing to set the value of an attribute that is a foreign key to "" (empty string) instead of `null`. For example `<select>` elements may return an empty string to signalize `null`. But Eloquent does not convert it to  `null` automatically. Create a [mutator](http://laravel.com/docs/5.1/eloquent-mutators) to solve the problem. Example: [Stack Overflow link](http://stackoverflow.com/questions/23450953/laravel-nullable-foreign-keys#23451155)

## Captcha image not displayed

Make sure you PHP's GD lib is installed: Login to your website, go to the admin interface, then in the navigation go to "website" and then to "settings". Click on "PHP Info" and search for "GD Support". If you cannot find this string, GD lib is not installed. 

If you use PHP 5 with Ubuntu you can install it with this console command: `sudo apt-get install php5-gd`

## Form field value not saved

If the value of a form field is not saved check if the name of the field is listed in the `fillable` array of the model.

## Issues with modules

When you add a new module in Contentify 2, you have to run this console command: `php artisan module:optimize`

Read more about it [in this issue](https://github.com/Contentify/Contentify/issues/101) and [this release notes](https://github.com/caffeinated/modules/releases/tag/v3.0).

## The website is slow

Try to disable those modules your websites does not use. Open the `app/Modules/ModuleName/module.json` of each of those modules and set the `enabled` property to `false`. Then clear the application cache: `php artisan cache:clear`

## Class not found

If you created a new class and the CMS keeps throwing an exception and telling you that it cannot find the class, then there might be something wrong with the namespace of the class or with its path. Contentify supports and prefers [PSR-4 autoloading](https://www.php-fig.org/psr/psr-4/). Therefore the namespace of a class has to match the path to the class. For example, the class `Objects\Machines\Car` should be in the file `any_path/Objects/Machines/Car.php`. If the namespace and path of your class are valid but the issue does not disappear, run this console command in the Contentify directory: `php composer.phar dumpautoload` This will make the CMS search for unknown PHP classes and remember where they are located.

## I can't use route caching

True. That's a limitation of the Laravel framework. It can only cache controller based routes, but the CMS uses global Closure routes as well. The good news is, it does not matter. Our research has shown that activating the route cache (after temporarily removing the Closure routes) does not speed up the execution time.

## escapeshellarg exception

The exception means that your web space hoster has restricted access to the command `escapeshellarg`. Please ask your hoster to grant access to this commnd since [it is not dangerous](http://stackoverflow.com/questions/23866753/is-escapeshellarg-safe-enough#answer-23867545). Unfortunately this command is part of a third party package, Symfony. Therefore we cannot simply create a workaround.

(Linked to: [issue 50](https://github.com/Contentify/Contentify/issues/50) )

## Register globals exception

If a "Directive 'register_globals' is no longer available in PHP" exception is thrown please check your `php.ini`. Make sure `register_globals` is set to `off`.